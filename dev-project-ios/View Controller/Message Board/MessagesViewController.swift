//
//  MessagesViewController.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 11/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit
import FirebaseCore
import RxSwift
import RxCocoa
import Rswift

class MessagesViewController: UIViewController {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageInput: UITextField!
    @IBOutlet weak var sendMessageBtn: UIButton!
    
    let reuseIdentifier = "messageCell"
    let viewModel = MessagesViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        configureTableView()
    }
    
    func setUp() {
        tableView.delegate = self
        tableView.dataSource = self
        messageInput.delegate = self
    }
    
    @objc func closeKeyboard() {
        messageInput.endEditing(true)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
        tableView.addGestureRecognizer(tapGesture)
    }
}

extension MessagesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MessageCell
        
        let testArray = [
            "First Message",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "Third Message",
            "Fourth Message",
            "Fifth Message"
        ]
        
        cell.userName.text = "Random User"
        cell.messageText.text = testArray[indexPath.row]
        cell.userImage.image = UIImage(named: "user-100")
        return cell
    }
}

extension MessagesViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.hidesBarsWhenKeyboardAppears = true
            self.heightConstraint.constant = 326
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.isNavigationBarHidden = false
            if self.heightConstraint.constant == 326 {
                self.heightConstraint.constant = 50
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
