//
//  LoginViewModel.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 05/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseAuth
import RxSwift

class LoginViewModel {
   
    var emailText = Variable<String>("")
    var passwordText = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(emailText.asObservable(), passwordText.asObservable()) { email, password in
            email.count >= 0 && password.count >= 5
        }
    }
}
