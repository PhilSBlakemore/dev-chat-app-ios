//
//  LoginViewController.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 05/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel = LoginViewModel()
    var handle: AuthStateDidChangeListenerHandle?
    let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityIndicator.stopAnimating()
        
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if Auth.auth().currentUser != nil {
                self.emailInput.text = user?.email
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        bindView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setUp() {
        view.backgroundColor = UIColor.appLight
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboards)))
    }
    
    func bindView() {
        //_ = emailInput.rx.text.map { $0 ?? "" }.bind(to: viewModel.emailText)
        _ = passwordInput.rx.text.map { $0 ?? "" }.bind(to: viewModel.passwordText)
        _ = viewModel.isValid.bind(to: loginBtn.rx.isEnabled)
        
        _ = viewModel.isValid.subscribe(onNext: { isValid in
            self.loginBtn.backgroundColor = isValid ? UIColor.appSuccess : UIColor.appError
        })
    }
    
    @IBAction func loginBtnPressed(_ sender: UIButton) {
        self.activityIndicator.startAnimating()
        loginUser()
    }
    
    fileprivate func loginUser() {
        Auth.auth().signIn(withEmail: emailInput.text!, password: passwordInput.text!) { (user, error) in
            if error != nil {
                self.activityIndicator.stopAnimating()
                self.alert(title: "Login Error", message: (error?.localizedDescription)!)
                return
            }
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Messages")
            self.navigationController!.pushViewController(viewController!, animated: true)
        }
    }
    
    @objc fileprivate func dismissKeyboards() {
        emailInput.resignFirstResponder()
        passwordInput.resignFirstResponder()
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.hidesBarsWhenKeyboardAppears = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension UIViewController {
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
