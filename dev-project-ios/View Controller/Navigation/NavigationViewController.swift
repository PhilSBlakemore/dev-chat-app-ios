//
//  NavigationViewController.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 08/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        navigationBar.barTintColor = UIColor.appLight.withAlphaComponent(0.50)
    }
}
