//
//  RegisterViewController.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 11/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth
import RxSwift
import RxCocoa

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var firstNameInput: UITextField!
    @IBOutlet weak var surnameInput: UITextField!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel = RegisterViewModel()
    let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityIndicator.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        bindView()
    }
    
    func setUp() {
        self.title = "Register"
        view.backgroundColor = UIColor.appLight
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboards)))
    }
    
    func bindView() {
        _ = firstNameInput.rx.text.map { $0 ?? "" }.bind(to: viewModel.firstNameText)
        _ = surnameInput.rx.text.map { $0 ?? "" }.bind(to: viewModel.surnameText)
        _ = emailInput.rx.text.map { $0 ?? "" }.bind(to: viewModel.emailText)
        _ = passwordInput.rx.text.map { $0 ?? "" }.bind(to: viewModel.passwordText)
        _ = viewModel.isValid.bind(to: signUpBtn.rx.isEnabled)
        
        _ = viewModel.isValid.subscribe(onNext: { isValid in
            self.signUpBtn.backgroundColor = isValid ? UIColor.appSuccess : UIColor.appError
        })
    }
    
    @IBAction func signUpBtnPressed(_ sender: UIButton) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        registerUser()
    }
    
    fileprivate func registerUser() {
        Auth.auth().createUser(withEmail: emailInput.text!, password: passwordInput.text!) { (user, error) in
            if error != nil {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.alert(title: "Registration Error", message: (error?.localizedDescription)!)
                return
            }
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Messages")
            self.navigationController!.pushViewController(viewController!, animated: true)
        }
    }
    
    @objc fileprivate func dismissKeyboards() {
        firstNameInput.resignFirstResponder()
        surnameInput.resignFirstResponder()
        emailInput.resignFirstResponder()
        passwordInput.resignFirstResponder()
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.hidesBarsWhenKeyboardAppears = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
