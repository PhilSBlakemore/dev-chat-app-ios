//
//  RegisterViewModel.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 11/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseAuth
import RxSwift

class RegisterViewModel {
    
    var firstNameText = Variable<String>("")
    var surnameText = Variable<String>("")
    var emailText = Variable<String>("")
    var passwordText = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(firstNameText.asObservable(), surnameText.asObservable(), emailText.asObservable(), passwordText.asObservable()) { firstName, surname, email, password in
            firstName.count >= 1 && surname.count >= 1 && email.count >= 5 && password.count >= 5
        }
    }
}
