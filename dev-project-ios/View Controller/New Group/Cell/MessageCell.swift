//
//  MessageCell.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 13/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var messageContainer: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var messageText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
