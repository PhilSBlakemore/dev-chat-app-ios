//
//  Message.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 11/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import Foundation

class Message {
    let subject: String
    let text: String
    let date: Date
    let sent: Bool
    let received: Bool
    
    init(subject: String, text: String, date: Date, sent: Bool, received: Bool) {
        self.subject = subject
        self.text = text
        self.date = date
        self.sent = sent
        self.received = received
    }
}
