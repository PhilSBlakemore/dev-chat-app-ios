//
//  User.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 11/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import Foundation

class User {
    let firstName: String
    let lastName: String
    let email: String
    
    init(firstNsme: String, lastName: String, email: String) {
        self.firstName = firstNsme
        self.lastName = lastName
        self.email = email
    }
}
