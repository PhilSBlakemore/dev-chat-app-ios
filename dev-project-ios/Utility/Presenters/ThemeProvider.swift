//
//  ThemeProvider.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 07/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

struct DefaultTheme {
    
    var background: UIColor {
        return UIColor.appBackground
    }
    
    var lightBackground: UIColor {
        return UIColor.appLight
    }
    
//    var navigation: UIColor {
//        return UIColor.primaryBlack
//    }
    
//    var navigationSelection: UIColor {
//        return UIColor.primaryYellow.withAlphaComponent(0.60)
//    }
    
//    var accent: UIColor {
//        return UIColor.
//    }
    
    var error: UIColor {
        return UIColor.red
    }
    
//    var selection: UIColor {
//        return UIColor.
//    }
    
    var selectionLight: UIColor {
        return UIColor.appLight.withAlphaComponent(0.60)
    }
    
//    var selectionDark: UIColor {
//        return UIColor.
//    }
    
    var titleLight: UIColor {
        return UIColor.appLight
    }
    
//    var subtitleLight: UIColor {
//        return UIColor.
//    }
    
    var accessoryLight: UIColor {
        return UIColor.appLight
    }
    
//    var titleDark: UIColor {
//        return UIColor.
//    }
    
//    var subtitleDark: UIColor {
//        return UIColor.
//    }
    
    var accessoryDark: UIColor {
        return UIColor.appBackground
    }
    
//    var border: UIColor {
//        return UIColor.
//    }
    
    func lightFont(withSize: CGFloat = 16) -> UIFont {
        return UIFont.systemFont(ofSize: withSize, weight: UIFont.Weight.light)
    }
    
    func regularFont(withSize: CGFloat = 16) -> UIFont {
        return UIFont.systemFont(ofSize: withSize, weight: UIFont.Weight.medium)
    }
    
    func boldFont(withSize: CGFloat = 16) -> UIFont {
        return UIFont.systemFont(ofSize: withSize, weight: UIFont.Weight.bold)
    }
}
