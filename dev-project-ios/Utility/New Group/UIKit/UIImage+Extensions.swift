//
//  UIImage+Extensions.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 07/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

extension UIImage {
    
    func base64Encode(compression: CGFloat = 1) -> String? {
        let imageData = UIImageJPEGRepresentation(self, compression)
        let base64 = imageData?.base64EncodedString(options: .lineLength64Characters)
        return base64
    }
    
    func scaleImage(toSize size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        if let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return newImage;
        }
        return nil
    }
    
    func scaleImage(byMultiplicationFactorOf factor: CGFloat) -> UIImage? {
        let size = CGSize(width: self.size.width*factor, height: self.size.height*factor)
        return scaleImage(toSize: size)
    }
    
    func compressTo(toSizeInMB size: Double) -> UIImage? {
        let bytes = size * 1024 * 1024
        let sizeInBytes = Int(bytes)
        var needCompress: Bool = true
        var imgData: Data?
        var compressingValue: CGFloat = 1.0
        
        while (needCompress) {
            if let resizedImage = scaleImage(byMultiplicationFactorOf: compressingValue), let data: Data = UIImageJPEGRepresentation(resizedImage, compressingValue) {
                if data.count < sizeInBytes || compressingValue < 0.1 {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.22
                }
            }
        }
        
        if let data = imgData {
            print("Finished with compression value of: \(compressingValue)")
            return UIImage(data: data)
        }
        return nil
    }
}
