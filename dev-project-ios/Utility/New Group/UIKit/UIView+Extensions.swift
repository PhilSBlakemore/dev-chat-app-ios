//
//  UIView+Extensions.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 07/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    var shadowHorizontalOffset: CGFloat {
        get {
            return layer.shadowOffset.width
        }
        set {
            layer.shadowOffset = CGSize(width: newValue, height: shadowVerticalOffset)
            layer.masksToBounds = newValue == 0
            setShadowProperties()
        }
    }
    
    var shadowVerticalOffset: CGFloat {
        get {
            return layer.shadowOffset.height
        }
        set {
            layer.shadowOffset = CGSize(width: shadowHorizontalOffset, height: newValue)
            layer.masksToBounds = newValue == 0
            setShadowProperties()
        }
    }
    
    private func setShadowProperties() {
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.2
        layer.shadowColor = UIColor.black.cgColor
    }
}
