//
//  UIColor+Extensions.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 07/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3:
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6:
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8:
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    static var primaryColor: UIColor {
        return UIColor(hexString: "7D81CA")
    }
    static var secondaryColor: UIColor {
        return UIColor(hexString: "C569A7")
    }
    static var tertiaryColor: UIColor {
        return UIColor(hexString: "94CFD7")
    }
    static var appBackground: UIColor {
        return UIColor(hexString: "2F485B")
    }
    static var appLight: UIColor {
        return UIColor(hexString: "F4F5F7")
    }
    static var appError: UIColor {
        return UIColor(hexString: "EA6E75")
    }
    static var appSuccess: UIColor {
        return UIColor(hexString: "7AD3BE")
    }
}
