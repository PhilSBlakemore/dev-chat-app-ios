//
//  UITextField+Extensions.swift
//  dev-project-ios
//
//  Created by Philip Blakemore on 07/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

extension UITextField {
    
    func becomeFirstResponderWithDelay(delay: Double = 0.05) {
        perform(
            #selector(becomeFirstResponder),
            with: nil,
            afterDelay: delay
        )
    }
}
